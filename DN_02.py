import time
import random
import matplotlib.pyplot as plt


def euclid(a, b):
    # print("E", a, b)
    if b <= 1:
        return b
    if a-b == 0:
        return b
    return euclid(max(b, a-b), min(b, a-b))


def non_recursive_euclid(a, b):
    while a > 1 and b > 1:
        c = a % b
        if c == 0:
            return b
        a, b = max(b, c), min(b, c)
    return 1


def choose_numbers(i):
    a = "1"
    b = "1"
    for _ in range(i-1):
        a += random.choice(["0", "1"])
        b += random.choice(["0", "1"])
    a = int(a, 2)
    b = int(b, 2)
    #a = random.getrandbits(i-1)
    #b = random.getrandbits(i-1)
    #print("{0:b}".format(a),"{0:b}".format(b))
    #print(a, b)
    #print()
    return a, b


def main(rng):
    times = []
    n_per_iter = 5
    for i in rng:
        tmp_time = 0
        print("Running Euclid ", i)
        for _ in range(n_per_iter):
            a, b = choose_numbers(i)
            #print(a, b)
            st = time.time()

            res = non_recursive_euclid(max(a, b), min(a, b))
            tmp_time += (time.time() - st)/n_per_iter

        times.append(tmp_time)
        if times[-1] > 2:
            break
    print(times)
    return times


def plot_it(times, rng):

    fig, ax = plt.subplots()
    ax.plot(rng[:len(times)], times)

    ax.set(xlabel='n-bits', ylabel='time (s)',
           title='Plot')
    ax.grid()

    fig.savefig("test.png")
    plt.savefig('times_2.png')
    #plt.show()


if __name__ == '__main__':
    print("Hello DN_02")
    rng = range(10, 100000, 1000)
    times = main(rng)
    plot_it(times, rng)
    #for i in range(19):
    #    choose_numbers(10)
    #print(euclid(264, 210))
    #print(non_recursive_euclid(264, 210))
    '''
        [0.0, 0.007796859741210938, 0.025800800323486327, 0.05819592475891113, 0.09240117073059083, 0.13559932708740235, 0.1974039554595
        9472, 0.26419644355773925, 0.33320231437683107, 0.4087996959686279, 0.48640065193176263, 0.5978047847747803, 0.7171961784362793,
         0.8237986564636229, 0.9546011447906495, 1.0959996700286865, 1.2548000812530518, 1.3674002170562742, 1.5485997200012207, 1.70799
        93724822997, 1.938200855255127, 2.1142026901245115]
    '''

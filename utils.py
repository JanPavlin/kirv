from itertools import cycle
def encode(s):
    return tuple(ord(c) - 65 for c in s)

def decode(t):
    return ''.join(chr(n + 65) for n in t)

def encrypt(plaintext, key):
    return decode((p + k) % 26 for p, k in zip(encode(plaintext), cycle(encode(key))))

def decrypt(ciphertext, key):
    return decode((c - k) % 26 for c, k in zip(encode(ciphertext), cycle(encode(key))))

def index_of_coincidence(s):
    d = {}
    for c in s:
        d.setdefault(c, 0)
        d[c] += 1
    l = len(s)
    return sum(n * (n-1) for n in d.values()) / (l * (l-1))

def freq_analysis(sequence):
    all_chi_squareds = [0] * 26
    english_frequences = [0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015,
                          0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749,
                          0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758,
                          0.00978, 0.02360, 0.00150, 0.01974, 0.00074]
    for i in range(26):

        chi_squared_sum = 0.0

        #sequence_offset = [(((seq_ascii[j]-97-i)%26)+97) for j in range(len(seq_ascii))]
        sequence_offset = [chr(((ord(sequence[j])-97-i)%26)+97) for j in range(len(sequence))]
        v = [0] * 26
        # count the numbers of each letter in the sequence_offset already in ascii
        for l in sequence_offset:
            v[ord(l) - ord('a')] += 1
        # divide the array by the length of the sequence to get the frequency percentages
        for j in range(26):
            v[j] *= (1.0/float(len(sequence)))

        # now you can compare to the english frequencies
        for j in range(26):
            chi_squared_sum+=((v[j] - float(english_frequences[j]))**2)/float(english_frequences[j])

        # add it to the big table of chi squareds
        all_chi_squareds[i] = chi_squared_sum

    # return the letter of the key that it needs to be shifted by
    # this is found by the smallest chi-squared statistic (smallest different between sequence distribution and
    # english distribution)
    shift = all_chi_squareds.index(min(all_chi_squareds))
    return chr(shift + 97)

'''  
Never used functions: 
'''

def get_key_3(ciphertext, key_length):
    ciphertext = ciphertext.upper()
    key = ''
    # Calculate letter frequency table for each letter of the key
    for i in range(key_length):
        sequence = ""
        # breaks the ciphertext into sequences
        for j in range(0,len(ciphertext[i:]), key_length):
            sequence += ciphertext[i+j]
        key += freq_analysis(sequence)
    print(key)
    return key



def get_key_2(text, key_length):
    text = text.upper()
    parts = [text[i::key_length] for i in range(key_length)]
    C = parts
    print([index_of_coincidence(y) for y in parts])
    #we need to figure out parts[0]

    sort = list()

    print(sorted(((index_of_coincidence(parts[0] + decrypt(parts[1], chr(65 + i))), i) for i in range(26)), reverse=True))
    sort.append(sorted(((index_of_coincidence(parts[1] + decrypt(parts[0], chr(65 + i))), i) for i in range(26)), reverse=True))
    sort.append(sorted(((index_of_coincidence(parts[0] + decrypt(parts[1], chr(65 + i))), i) for i in range(26)), reverse=True))
    sort.append(sorted(((index_of_coincidence(parts[0] + decrypt(parts[2], chr(65 + i))), i) for i in range(26)), reverse=True))
    sort.append(sorted(((index_of_coincidence(parts[0] + decrypt(parts[3], chr(65 + i))), i) for i in range(26)), reverse=True))
    sort.append(sorted(((index_of_coincidence(parts[0] + decrypt(parts[4], chr(65 + i))), i) for i in range(26)), reverse=True))

    sortede = sorted(((index_of_coincidence(parts[0] + decrypt(parts[1], chr(65 + i))), i) for i in range(26)),
                     reverse=True)
    for el in sortede:
        print(el)

    ccc = ''.join((''.join(t) for t in zip(C[0], decrypt(C[1], chr(65 + 15)), decrypt(C[2], chr(65 + 12)), decrypt(C[3], chr(65 + 11)), decrypt(C[4], chr(65 + 7)))))
    decripts = [decrypt(ccc, chr(65 + i)) for i in range(26)]

    return sort


def get_ind(key):
    import string
    l = list()
    for i in key:
        l.append(string.ascii_lowercase.index(i))
    return l
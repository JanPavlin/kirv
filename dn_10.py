import random
import string
import hashlib
solution = True
N = 10


def print_hex(string=None):
    # encode into hexadecimal values

    if string is None:
        first_string = "abc"
        second_string = "xyz"
        print(first_string.encode('ASCII').hex())
        print(second_string.encode('ASCII').hex())
    else:
        print(string.encode('ASCII').hex())
        return string.encode('ASCII').hex()


if __name__ == '__main__':
    print("Hello DN_10")

    hashes = dict()
    if not solution:
        while True:
            if len(hashes) % 1000000 == 0:
                print(len(hashes))
            s = bytes(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N)), 'ASCII')
            H = hashlib.sha256(s).hexdigest()[52:]

            if H in hashes.keys():
                if s in hashes[H]:
                    continue
                else:
                    print("najdu!")
                    print(s)
                    print(hashes[H])
                    exit()
            else:
                hashes[H] = s
            #print(hashes)
    else:
        print()
        sol_1 = bytes('6LP0DFORIF', 'ASCII')
        H_1 = hashlib.sha256(sol_1).hexdigest()[52:]

        sol_2 = bytes('JSLFRB1LF0', 'ASCII')
        H_2 = hashlib.sha256(sol_2).hexdigest()[52:]

        print(H_1, H_2)

        print("Resitev:")

        print_hex("6LP0DFORIF")

        print_hex("JSLFRB1LF0")

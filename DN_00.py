def check_density(E):
    length = len(E)
    vowels = 0
    for char in E:
        if char in 'aeiou':
            vowels += 1
    print("Length of text =", length)
    print("Ratio of vowels is", "{:.3f}".format(vowels/length) + "%.")


def get_keysize(E, EE):
    for i in range(1, 10):
        for j in range(0, len(E), i):
            #if len(set(E[j: j + i])) == i and i == 6:
            #    print(E[j: j+i], EE[j: j+i])
            subset_E = set(E[j: j+i])
            subset_EE = set(EE[j: j+i])

            if subset_E != subset_EE:
                break

            if j >= len(E)-i:
                return i

def decipher(E, keysize):
    result = ""
    for j in range(0, len(E), keysize):
        part = E[j: j + keysize]
        result += part[3] + part[1] + part[0] + part[4] + part[2] + part[5]
    return result


if __name__ == '__main__':
    print("Hello DN00")

    E = 'arjkljmileenvsjaoervumtnzieerčoneelpajlobakojniedolriaalaztjobkalaijnahilonkiimogsolodiibtsvkiaotuojrj' \
        'nemeoakjlnaočonnidjnpaeoatlsvjklradesesvutžadreazisbaalitčeatudtoaduntiaobjjlštsairžkinniimogsolodiibt' \
        'inoikgkrbaiidolhipajooblpajkaeneavdrnakmjasoelldejunjtpoeronsvtoeoničnipamaoglnioovjikoassežrebalihnio' \
        'tindiartžsao'
    EE = 'jrlakjliemenjsovaeuvtrmneirzečenloepljbaoajoikneloidraaltazjkbloaanjhiainoilkigoomslidboitkvasioourtj' \
         'jmeonealjaknonoičndpnejaoltvasjrldkaeseusvtdaežrasiazbatielčaduottanuidtajblojšasrtižniiknigoomslidbo' \
         'itonkiigbrikailoidhpojbaoljaapkeaednvrkajnmaeolsldujjenteooprntvesoočiinnpamgaoloivnojoksiasržbeeahii' \
         'lnoniitdažtarso'

    check_density(E)
    check_density(EE)

    keysize = get_keysize(E, EE)
    print("Keysize is " + str(keysize) + ".")

    #   key:
    #(1 2 3 4 5 6)
    #(4 2 1 5 3 6)
    m = decipher(E, keysize)
    print(m)

    # message:
    #Kralj je imel na svojem vrtu neizrečeno lepo jablano ki je rodila zlata jabolka
    # a jih nikoli niso mogli dobiti. Vsako jutro je eno manjkalo. Noč in dan je postavljal
    # k drevesu straže, da bi zasačil etatu toda tudi najboljši stražniki niso mogli dobiti
    # nikogar, ki bi hodil po jabolka pa je vendar manjkalo slednje jutro po eno vse to ni
    # nič pomagalo in vojaki so se že branili hoditi na stražo.


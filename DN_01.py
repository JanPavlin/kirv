
import matplotlib.pyplot as plt
def shift(s, n):
    return ''.join(chr((ord(char) - 97 + n) % 26 + 97) for char in s)

def gcd(list):
    import functools as f
    g = lambda a, b: a if b == 0 else g(b, a % b)  # Gcd for two numbers
    return f.reduce(lambda x, y: g(x, y), list)


def index_of_coincidence(s):
    d = {}
    for c in s:
        d.setdefault(c, 0)
        d[c] += 1
    l = len(s)
    return sum( n * (n-1) for n in d.values()) / (l * (l-1))


def get_key_length(E):
    scores = []
    options = []
    options.append(E)
    for i in range(1, 100):
        text = ""
        for j in range(i):
            text += " "
        text += E[:-i]
        options.append(text)
    #[print(len(el)) for el in options]
    for i in range(1, len(options)):
        counter = 0
        option = options[i]
        for j in range(len(option)):
            if options[0][j] == option[j]:
                counter += 1
        scores.append(counter)

    plt.plot(scores)
    plt.ylabel('Število enakih elementov')
    plt.xlabel('Zamik')
    plt.show()

    # print(scores)
    m = max(scores)
    best = [i for i, j in enumerate(scores) if j == m]
    differences = []
    print(best)
    for i in range(len(best)-1):
        differences.append(best[i+1] - best[i])
    key_length = gcd(differences)

    print("Finding length of the key: ")
    print("maximum matches = ", m)
    print("That happened in shift of", differences, "appart")
    print("keylength is GCD of those differences =", key_length)
    return key_length


def get_freq(text):
    textlen = len(text)
    all_freq = {}
    for i in text:
        if i in all_freq:
            all_freq[i] += 1/textlen
        else:
            all_freq[i] = 1/textlen
    #print(all_freq)
    return all_freq


def multiply_freq(freq, letter_freq_en):
    score = 0
    for key in freq.keys():
        score += freq[key] * letter_freq_en[key]
    return score


def get_key(text, key_length, letter_freq_en):
    parts = [text[i::key_length] for i in range(key_length)]

    scores = []
    for part in parts:
        score = []

        for i in range(26):
            new_part = shift(part, -i)
            # print(i, "sft", new_part)

            freq = get_freq(new_part)
            s = multiply_freq(freq, letter_freq_en)
            score.append(s)
        scores.append(score)

    key = ""
    ind = []
    for score in scores:
        index = score.index(max(score))
        #print("index", index, "max",  max(score))
        key += shift("a", index)
        ind.append(index)
    print("KEY: ", key)
    return key, ind


def decipher_text(E, ind):
    original = ""
    while len(ind) < len(E):
        ind += ind
    #print(ind)
    for i in range(len(E)):
        original += shift(E[i], -ind[i])
    return original


if __name__ == '__main__':
    print("Hello DN01")
    E = 'weqrdxfbncqdsgqmisyrdvrntqeujqwyoqjoisncnffrbbesnapzgcjtrqrocczboiczxrvugboezzqjvzanuafbrmipvrbmehbdbrvhwbj' \
           'anwbfguxwkvvviicrkckvrmqubbckfarkitycnzyocbafarjktwqnvkvnbjvtnutvburujovmbysxrvxoambysanfkrnhpvgrwbfigcefab' \
           'amyiacadsafpfkramkcfniiquowivvvjlhgqmphbxakolnlrknhbysaxvkvrcpzfqmipvrbmehsxzrzyqqjvhwbjarwierfjqugpxcihune' \
           'ycynnffrbbkvexcxvnwlucaxbxwincgiacqcmrqimssxcernutkvenmsigxnkvrbmrzfxvfbrlidsuxuvotjqeoamwwhunxrqxxnychwljk' \
           'urkyhungyoqcibsafqkvgqmdbbwmnsenavsavwissawdhujbkwznnffgqvfcanefiymiemyxvxseemehhamzbgxbyssxzvggjvuwguiphun' \
           'zvwammvdfcqczanajoamafzvccusnwlecgqqeujjajsrwwwwgkckgbvmkwznarbrjocsbaiyojtncmvwofjraqk'

    letter_freq_en = {'e': 0.1270, 't': 0.0906, 'a': 0.0817, 'o': 0.0751, 'i': 0.0697, 'n': 0.0675, 's': 0.0633,
                      'h': 0.0609, 'r': 0.0599, 'd': 0.0425, 'l': 0.0403, 'c': 0.0278, 'u': 0.0276, 'm': 0.0241,
                      'w': 0.0236, 'f': 0.0223, 'g': 0.0202, 'y': 0.0197, 'p': 0.0193, 'b': 0.0129, 'v': 0.0098,
                      'k': 0.0077, 'j': 0.0015, 'x': 0.0015, 'q': 0.0010, 'z': 0.0007}


    key_length = get_key_length(E)
    key, ind = get_key(E, key_length, letter_freq_en)
    deciphered_text = decipher_text(E, ind)
    print(deciphered_text)

    '''correct_key, ind = get_key(E, key_length, letter_freq_en)
    
    for i in range(1, 30):
    key_length = i
    correct_key, ind = get_key(E, key_length, letter_freq_en)
    deciphered_text = decipher_text(E, correct_key, ind)'''


    '''
    Once upon a time there lived a king who had a great forest near his palace full of all kinds of wild animals. 
    One day he sent out a huntsman to shoot him a roe but he did not come back. Perhaps some accident has befallen him 
    said the king and the next day he sent out two more huntsmen who were to search for him. But they too stayed away 
    then on the third day he sent for all his huntsmen and said scour the whole forest through and donot give up until 
    ye have found all three but of the se also none came home again and of the pack of hounds which they had taken with
    them none were seen more from that time forth. No one would any longer venture into the forest
    and it lay there in deep stillness and solitude and nothing was seen of it but sometimes an eagle or a hawk flying
    over it
    '''

